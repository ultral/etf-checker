#!/bin/bash

readonly script_dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
readonly values_file="$script_dir/values.csv"
readonly separator=";"
readonly url_new_line="%0A"
readonly url_space="%20"
readonly telegram_https_api_key="5358064143:AAFb0isHUAJujA6MN9KvfVrSzDGjkD9wSzU"
readonly etf_info_chat_id="-744635010"
#readolny telegram_bot="t.me/etf_info_bot"

send_telegram_message() {
  local params=$*
  params=$(echo "$params" | sed "s/ /%20/g")
  local message="https://api.telegram.org/bot$telegram_https_api_key/sendMessage?chat_id=$etf_info_chat_id&text=$params"
  curl -s "$message"
}

read_isins_from_file() {
  #-F ';' -> line separator
  #if (NR>1) -> omits first line
  #print $1 -> prints first column
  local etf_isins=$(awk -F "$separator" '{if (NR>1) print $1}' "$values_file" | sort | uniq)
  echo "$etf_isins"
}

read_isin_dates_and_values() {
  local isin=$1
  local values=$(grep "$isin" "$values_file" | awk -F "$separator" '{print $3,$2}')
  echo $values
}

get_etf_isins() {
  local etf_isins="IE00B5BMR087 IE00BD1F4N50 IE00B579F325 IE00B4NCWG09 LU0908500753 IE00BMVB5R75 IE00BK5BQT80 IE00BYZ28W67"
  if [ -f "$values_file" ]; then
    etf_isins=$(read_isins_from_file "$values_file")
  else
    echo "ISIN;VALUE;DATE_TIME" >>"$values_file"
  fi
  echo "$etf_isins"
}

get_file_name() {
  echo "$values_file"
}

build_newline() {
  local isin=$1
  local value=$2
  local date_time=$(date +"%Y-%m-%d_%T")
  echo "$isin$separator$value$separator$date_time"
}

write_to_file() {
  local isin=$1
  local value=$2
  local new_line=$(build_newline $isin $value)
  local file_name=$(get_file_name $isin)
  echo $new_line >>"$file_name"
}

get_url() {
  local isin=$1
  local url="https://www.justetf.com/en/etf-profile.html?isin=$isin"
  echo "$url"
}

download_etf_value() {
  local isin=$1
  local url=$(get_url $isin)
  echo -n "ISIN: $isin"
  local page="$(curl -s $url)"
  #this gets numer in format: for 123,23
  local value=$(echo "$page" | grep "<div class=\"infobox mb-0\">" -A 5 | grep "<span>" | sed 's/[^0-9.]//g;/^[[:space:]]*$/d;s/\./,/g')
  if [ -z "$value" ]; then
    echo " value: IS EMPTY"
  else
    echo " value: $value"
    write_to_file "$isin" "$value"
  fi
}

download_new_values() {
  local etf_isins=$*
  for isin in $etf_isins; do
    download_etf_value $isin
  done
}

get_prefix_filter() {
  local filter=$1
  local prefix_filer=$(date -d "$filter" +"%Y-%m-%d")
  echo "$prefix_filer"
}

filter_values() {
  #first param should be prefix_filer
  local prefix_filer=$1;shift;
  local params_num=$#
  local params_array=("$@")
  local filtered_values=""

  for ((i = 0; i < params_num; i += 2)); do
    if [[ "${params_array[i]}" == $prefix_filer* ]]; then
      #echo "${params_array[i]} mach $prefix_filer - value: ${params_array[i+1]}"
      filtered_values+="${params_array[i+1]} "
    fi
  done
  #todo add sort | uniq ?
  echo "$filtered_values"
}

count_avg() {
  local input_array=$*
  local params_num=$#
  local avg="0"
  if [ "$params_num" -gt 0 ]; then
    #this get avg (sum/num) in 0.00 precision (OFMT = "%0.2f")
    avg=$(echo $input_array | awk '{sum=0;num=0; for (i=1; i<=NF; i++) { gsub(",",".",$i); sum+=$i; num++ } OFMT = "%0.2f"; print sum/num}' | sed 's/\./\,/g')
  fi
  echo "$avg"
}

get_last() {
  echo "${@:$#}"
}

percent_difference() {
  local old=$1
  local new=$2
  local percent="0"
  if [ -n "$old" -a -n "$new" -a "$old" != "0" -a "$new" != "0" ]; then
   percent=$(echo "$old;$new" | awk -F "$separator" '{ gsub(",",".",$1); gsub(",",".",$2); OFMT="%0.2f";  print (($2-$1)/$1)*100}' | sed 's/\./\,/g')
  fi
  echo "$percent"
}

get_values_change_message() {
  local avg=$1;local last=$2;local percent_diff=$3;local send_threshold="1"
  local is_ok=$(echo "$percent_diff$separator$send_threshold" | awk -F "$separator" 'function abs(x){return ((x < 0.0) ? -x : x)} { gsub(",",".",$1); if ($2>abs($1)) print "no"; else print "yes";}' )
  local print=""
  if [ "$is_ok" == "yes" ]; then
    print="yesterday avg: ${avg}, current value: ${last} change: $percent_diff%"
  fi
  echo "$print"
}

check_values_change() {
  local all_values=$@
  local prefix_filter=$(get_prefix_filter "1 days ago")
  local filtered_values=$(filter_values $prefix_filter $all_values)
  local avg=$(count_avg $filtered_values)
  local last=$(get_last $all_values)
  local percent_diff=$(percent_difference $avg $last)
  local message=$(get_values_change_message $avg $last $percent_diff);
  echo "$message"
}

check_etf_value_change() {
  local isin=$1
  local values=$(read_isin_dates_and_values "$isin")
  local message=$(check_values_change "$values")

  if [ -n "$message" ]; then
    local url=$(get_url $isin)
    echo "$message -> $url$url_new_line"
  fi
}

proces_etf_changes() {
  local etf_isins=$*
  local telegram_message=""
  for isin in $etf_isins; do
    telegram_message+="$(check_etf_value_change $isin)"
  done

  if [ -n "$telegram_message" ]; then
    send_telegram_message "$telegram_message"
  fi
}

check_online() {
  local retry_count=6
  local online=""
  local site="http://www.justetf.com"

  for (( i=1; i<=$retry_count; i++ ))
  do
    online=$(curl -Is "$site" | head -1)
    if [ -n "$online" ]; then
      #we are online
      return 0
    fi
    sleep "10s"
  done
  echo -e "Connection to: $site is not possible\nFINISH"
  exit 1
}

readonly all_etf_isins=$(get_etf_isins)
echo "START values_downloader $(date +"%Y-%m-%d_%T")"
check_online
download_new_values "$all_etf_isins"
proces_etf_changes "$all_etf_isins"
echo -e "\nFINISH $(date +"%Y-%m-%d_%T")"
